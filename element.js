 //Object elelemt to create tag HTML follow struture <ul><li>...do sonmething...<li>ul>

 myModules.define("ELEMENT", [], function () {
                // contruct object element 
                function elementFunction(content, state, obj) //obj param is instance of class Task 
                {
                    // contruct HTML tags
                    this.li = document.createElement("LI");
                    this.div = document.createElement("DIV");
                    this.label = document.createElement("LABEL");
                    this.checkbox = document.createElement("INPUT");
                    this.remove = document.createElement("A");
                    this.edit = document.createElement("INPUT");


                    this.checkbox.setAttribute("type", "checkbox");
                    this.checkbox.checked = state;
                    this.edit.setAttribute("type", "text");
                    this.edit.setAttribute("class", "edit");
                    this.li.setAttribute("class","list-group-item");

                    this.label.innerHTML = content;
                    this.remove.innerHTML = "delete";

                    this.obj = obj;
                }
                // get tag div
                elementFunction.prototype.getDiv = function ()
                {
                    return this.div;
                };
                // set value for label
                elementFunction.prototype.setLabel = function (content)
                {
                    this.label.innerHTML = content;
                };
                // edit line of label when click checkbox
                elementFunction.prototype.editLabel = function ()
                {
                    var state = this.getCheckbox().checked;
                    if (state)
                    {
                        this.label.setAttribute("class", "lineThrough");
                    }
                    else
                    {
                        this.label.setAttribute("class", "lineNone");
                    }
                };
                // get label
                elementFunction.prototype.getLabel = function ()
                {
                    return this.label;
                };
                // get tag li
                elementFunction.prototype.getLi = function ()
                {
                    return this.li;
                };
                // set state of checkbox
                elementFunction.prototype.setCheckbox = function (state)
                {
                    this.checkbox.checked = state;
                };
                //get state of checkbox
                elementFunction.prototype.getCheckbox = function ()
                {
                    return this.checkbox;
                };
                // set remove tag
                elementFunction.prototype.setRemove = function ()
                {
                    this.remove.setAttribute("href", "#");
                    this.remove.setAttribute("class", "hide");
                };
                //get remove tag
                elementFunction.prototype.getRemove = function ()
                {
                    return this.remove;
                };
                // return instance of class task
                elementFunction.prototype.getObj = function ()
                {
                    return this.obj;
                };
                // get edit input
                elementFunction.prototype.getEdit = function ()
                {
                    return this.edit;
                };
                // edit Edit input
                elementFunction.prototype.editEdit = function ()
                {
                    this.edit.setAttribute("class", "hide");
                };
                // create element to add list
                elementFunction.prototype.getElement = function ()
                {
                    this.setRemove();
                    this.editLabel();
                    this.editEdit();
                    var obj = this.getObj();
                    var li = this.getLi();
                    var div = this.getDiv();
                    var checkbox = this.getCheckbox();
                    var label = this.getLabel();
                    var remove = this.getRemove();
                    var edit = this.getEdit();
                    div.appendChild(checkbox);
                    div.appendChild(label);
                    div.appendChild(remove);
                    li.appendChild(div);
                    li.appendChild(edit);

                    this.eventEdit(edit, obj);
                    this.eventLabel(label, obj);
                    this.eventLi(li, obj);
                    this.eventCheckbox(checkbox, obj);
                    this.eventRemove(remove, obj);
                    return li;
                };
                // event of Tag li
                elementFunction.prototype.eventLi = function (elm, obj)
                {
                    var self = this;
                    // event mouse over tag li
                    elm.addEventListener("mouseover", function (event) {
                        var remove = self.getRemove();
                        remove.setAttribute("class", "show");
                    });
                    // event mouse leave tag li 
                    elm.addEventListener("mouseleave", function (event) {
                        var remove = self.getRemove();
                        remove.setAttribute("class", "hide");
                    });
                };
                // event of label
                elementFunction.prototype.eventLabel = function (elm, obj)
                {
                    var self = this;
                    // event when double click to label
                    elm.addEventListener("dblclick", function (event) {
                        var edit = self.getEdit();
                        var div = self.getDiv();
                        div.setAttribute("class", "hide");
                        edit.setAttribute("class", "show");
                        edit.focus();
                    });
                };
                // event of tag edit input
                elementFunction.prototype.eventEdit = function (elm, obj)
                {
                    var self = this;
                    // event when focus input tag
                    elm.addEventListener("focus", function (event) {
                        var label = self.getLabel();
                        elm.value = label.textContent;
                    });
                    // event when leave edit input tag
                    elm.addEventListener("blur", function (event) {
                        var edit = self.getEdit();
                        var div = self.getDiv();
                        var label = self.getLabel();
                        var newValue = edit.value;
                        label.innerHTML = newValue;
                        div.setAttribute("class", "show");
                        edit.setAttribute("class", "hide");
                        obj.setContent(newValue);
                        obj.update();
                    });
                };
                // event of checkbox
                elementFunction.prototype.eventCheckbox = function (elm, obj)
                {
                    var self = this;
                    elm.addEventListener("click", function (event) {
                        var label = self.getLabel();
                        if (this.checked)
                        {
                            label.setAttribute("class", "lineThrough");
                        }
                        else
                        {
                            label.setAttribute("class", "lineNone");
                        }
                        obj.setState(elm.checked);
                        obj.update();
                    });
                };

                elementFunction.prototype.eventRemove = function (elm, obj)
                {
                    var self = this;
                    elm.addEventListener("click", function (event) {
                        event.preventDefault();
                        var Li = self.getLi();
                        Li.remove();
                        obj.removeObj();
                    });
                };

                return elementFunction;
            });
