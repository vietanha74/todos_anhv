 myModules.define("TASK", ["ELEMENT"], function (element) {
                function jobFunction(id, content, state) {
                    this.id = id;
                    this.content = content;
                    this.state = state | false;
                }

                jobFunction.prototype.getId = function ()
                {
                    return this.id;
                };

                jobFunction.prototype.setState = function (state)
                {
                    this.state = state;
                };

                jobFunction.prototype.getState = function ()
                {
                    return this.state;
                };

                jobFunction.prototype.update = function ()
                {
                    var id = this.getId();
                    var state = this.getState();
                    var content = this.getContent();
                    var storage = JSON.parse(localStorage[storeName]);
                    for (var i = 0; i < storage.length; i++)
                    {
                        if (id == storage[i].id)
                        {
                            if(storage[i].state != state)
                            {
                                storage[i].state = state;
                            }
                            else
                            if(storage[i].content != content)
                            {
                                storage[i].content = content;
                            }
                        }
                    }
                    localStorage[storeName] = JSON.stringify(storage);
                };

                jobFunction.prototype.setContent = function (content)
                {
                    this.content = content;
                };

                jobFunction.prototype.getContent = function ()
                {
                    return this.content;
                };
                
                jobFunction.prototype.getElement = function ()
                {
                    var elem = new element(this.getContent(), this.getState(), this);
                    return elem.getElement();
                };


                jobFunction.prototype.removeObj = function ()
                {
                    var id = this.getId();
                    var storage = JSON.parse(localStorage[storeName]);
                    for (var i = 0; i < storage.length; i++)
                    {
                        if (id === storage[i].id)
                        {
                            var index = storage.indexOf(storage[i]);
                            storage.splice(index, 1);
                        }
                    }
                    localStorage[storeName] = JSON.stringify(storage);
                };

                return jobFunction;
            });