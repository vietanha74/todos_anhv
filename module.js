 //Module Object use to call object or injection other object into object we need to use

 var myModules = (function () {
                var modules = {};

                function define(name, deps, impl)
                {
                    for (var i = 0; i < deps.length; i++)
                    {
                        deps[i] = modules[deps[i]];
                    }
                    modules[name] = impl.apply(impl, deps); //This is invoking the definition wrapper function for a module 
                }
                
                function get(name) {
                    return modules[name];
                }

                return {
                    define: define,
                    get: get
                };

            })();