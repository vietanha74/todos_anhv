     var storeName = "store"; // name of localstorage to containe data of app
            // Object to load any objects to use in process 
 
            myModules.define("LIST", ["TASK"], function (Task) {
                //get List 
                var list = document.getElementById("list");

                // generate random ID 
                function genID(range)
                {
                    return Math.round(Math.random() * range);
                }
                //create ID
                function createID(id)
                {
                    if (localStorage[storeName])
                    {
                        var store = JSON.parse(localStorage[storeName]);

                        if (store.length > 0)
                        {
                            var flag = true;
                            while (flag)
                            {
                                var id = genID(100);
                                for (var i = 0; i < store.length; i++)
                                {
                                    if (store[i].id === id)
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else
                                    {
                                        flag = false;

                                    }
                                }
                            }
                        }
                        else
                        {
                            var id = 1;
                        }
                    }
                    else
                    {
                        var id = 1;
                    }

                    return id;
                }
                //add to localStorage
                function addStore(task)
                {
                    if (localStorage[storeName])
                    {
                        var store = JSON.parse(localStorage[storeName]);
                        store.push(task);
                        localStorage[storeName] = JSON.stringify(store);
                    }
                    else
                    {
                        localStorage[storeName] = JSON.stringify([task]);

                    }
                }
                // init list when page loaded
                function initList()
                {
                    if (localStorage[storeName])
                    {
                        var storage = JSON.parse(localStorage[storeName]);
                        list.innerHTML = "";
                        if (storage)
                        {
                            for (var i = 0; i < storage.length; i++)
                            {
                                var id = storage[i].id;
                                var content = storage[i].content;
                                var state = storage[i].state;

                                var task = new Task(id, content, state);
                                list.appendChild(task.getElement());
                            }
                        }
                    }
                }
                // add element to list and object to localStorage
                function addList(value)
                {
                    var task = new Task(createID(), value);
                    addStore(task);
                    list.appendChild(task.getElement());
                }

                return {
                    addList: addList,
                    initList: initList
                };
            });
            
            //Oject to handle input when you input task and click enter
            myModules.define("INPUT", ["LIST"], function (list) {
                var self = document.getElementById("input");

                self.addEventListener("keypress", function (event) {
                    var _value = self.value;
                    if (event.which === 13 && _value !== "")
                    {
                        list.addList(_value);
                        self.value = "";
                    }
                });
            });
            
            // Object to remove checked 
            myModules.define("REMOVECHECKED",["TASK"],function(Task){
                var btn = document.getElementById("rmChecked");
                
                btn.addEventListener("click",function(event){
                    var storage = JSON.parse(localStorage[storeName]);
                   for(var i =0 ; i <storage.length ; i++)
                    {
                        if(storage[i].state==true)
                        {
                            var id = storage[i].id;
                            var content = storage[i].content;
                            var state = storage[i].state;
                            var task = new Task(id,content,state);
                            task.removeObj();
                        }
                    }
                    myModules.get("LIST").initList();
                });
            });

            myModules.get("LIST").initList();